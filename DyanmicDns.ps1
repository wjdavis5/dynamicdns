$ZONEID = "" #update
$TYPE = "A"

$RECORDSET = "" #update
$TTL = 300
$COMMENT = "Auto updating"

$result = iwr -Uri https://wtfismyip.com/text
$currentip = $result.Content.Replace("`n","").Replace("`r","")
$fileExists = Test-Path -Path "$env:temp\ip.txt"
$match = $false
if($fileExists){
    $oldIp = (gc -Path "$env:temp\ip.txt").Replace("`n","").Replace("`r","")
    $match = $currentip.ToString().Trim() -eq $oldIp.ToString().Trim()
}


if(!$match -or !$fileExists){

$json = @"
{
      "Comment":"$COMMENT",
      "Changes":[
        {
          "Action":"UPSERT",
          "ResourceRecordSet":{
            "ResourceRecords":[
              {
                "Value":"$currentip"
              }
            ],
            "Name":"$RECORDSET",
            "Type":"$TYPE",
            "TTL":$TTL
          }
        }
      ]
    }
"@
    Set-Content -Path "$env:temp\ip.txt" -Value $currentip
    $filename = "$env:temp\TEMP-$(Get-Date -format 'yyyy-MM-dd hh-mm-ss').jsv" 
    New-Item $filename -itemType File
    Set-Content -Path $filename -Value $json
    aws route53 change-resource-record-sets --hosted-zone-id $ZONEID --change-batch "file://$filename"
}